﻿using UnityEngine;
using UnityEngine.AI;

public class RandomSpawn : MonoBehaviour {

    [SerializeField]
    private GameObject agentPrefab;
    [SerializeField]
    private int count = 10;                 // Número de agentes que se instancian
    [SerializeField]
    private Material[] randomMaterials;
    [SerializeField]
    private Transform agentsParent;
    [SerializeField]
    private Transform[] destinations;       // Destinos de los agentes

    private Collider spawnArea;

    private void Awake() {
        Vector3 minBound;
        Vector3 maxBound;
        spawnArea = GetComponent<Collider>();
        minBound = spawnArea.bounds.min;
        maxBound = spawnArea.bounds.max;
        for (int i = 0; i < count; i++) {
            Vector3 randomPosition = new Vector3(
                Random.Range(-1f, 1f),
                Random.Range(-1f, 1f),
                Random.Range(-1f, 1f)
            );
            randomPosition = transform.TransformPoint(randomPosition * .5f);
            GameObject agent = Instantiate(agentPrefab, randomPosition,
                Quaternion.identity, agentsParent);
            if(randomMaterials.Length > 0) {
                agent.GetComponent<Renderer>().material = randomMaterials[
                Random.Range(0, randomMaterials.Length)];
            }
            if(destinations.Length > 0) {
                agent.GetComponent<NavMeshAgent>().destination = destinations[
                Random.Range(0, destinations.Length)].position;
            }
        }
    }
    
}
