﻿using UnityEngine;
/// <summary>
/// Adaptado de http://wiki.unity3d.com/index.php?title=Flocking
/// </summary>
public class BoidWatcher : MonoBehaviour {
    public Transform boidController;

    void LateUpdate() {
        if (boidController) {
            Vector3 watchPoint = boidController.GetComponent<BoidController>().flockCenter;
            transform.LookAt(watchPoint + boidController.transform.position);
        }
    }
}