﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomPositionsGenerator : MonoBehaviour {
    private const string FLAG_GAMEOBJECT_NAME = "GreenFlag";
    private const string FLAG_NAME = "flag";
    private const string RANDOM_TARGET_GAMEOBJECT_NAME = "GreenFlag";
    private const string RANDOM_TARGET_NAME = "randomTarget";
    private const string RANDOM_AREA_NAME = "Base";

    private GameObject flag;
    private GameObject randomTarget;
    private Transform randomArea;
    private BehaviorExecutor behaviorExecutor;
    private bool reachedDestination;
    private NavMeshAgent agent;

    private void Start() {
        List<string> objectParamsNames;
        behaviorExecutor = GetComponent<BehaviorExecutor>();
        objectParamsNames = behaviorExecutor.blackboard.objectParamsNames;
        flag = transform.GetChild(0).gameObject;
        randomTarget = transform.GetChild(1).gameObject;
        flag.transform.position = GameObject.Find(FLAG_GAMEOBJECT_NAME).transform.position;
        randomTarget.transform.parent = null;
        flag.transform.parent = null;
        randomArea = GameObject.Find(RANDOM_AREA_NAME).transform;
        agent = GetComponent<NavMeshAgent>();
    }
	
	void Update () {
        if(reachedDestination && agent.destination != flag.transform.position) {
            // Cambia aleatoramiente la posición de randomTarget
            Vector3 randomPosition = new Vector3(
                Random.Range(-1f, 1f),
                Random.Range(-1f, 1f),
                Random.Range(-1f, 1f)
            );
            randomPosition = randomArea.TransformPoint(randomPosition * .5f);
            randomTarget.transform.position = randomPosition;
            reachedDestination = false;
        } else {
            if(agent.remainingDistance <= 0.5f) {
                reachedDestination = true;
            }
        }
        
    }

    private void OnDestroy() {
        Destroy(flag);
        Destroy(randomTarget);
    }
}
