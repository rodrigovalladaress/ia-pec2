﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour {
    private const string GREEN_TEAM_NAME = "Equipo verde";
    private const string BLUE_TEAM_NAME = "Equipo azul";
    private const string DRAW = "Empate";

    [SerializeField]
    private Text greenTeamPointsText;
    [SerializeField]
    private Text blueTeamPointsText;
    [SerializeField]
    private Text winnerText;
    [SerializeField]
    private int pointsToWin = 20;

    private static Text s_GreenTeamPointsText;
    private static Text s_BlueTeamPointsText;
    private static Text s_WinnerText;
    private static int s_pointsToWin;

    private static int greenTeamPoints = 0;
    private static int blueTeamPoints = 0;
    private static bool checkForWinner = true;

    private void Awake() {
        s_GreenTeamPointsText = greenTeamPointsText;
        s_BlueTeamPointsText = blueTeamPointsText;
        s_WinnerText = winnerText;
        s_pointsToWin = pointsToWin;
    }

    public static void AddPoint(Boid.TypeOfBoid team) {
        switch (team) {
            case Boid.TypeOfBoid.Green:
                greenTeamPoints++;
                s_GreenTeamPointsText.text = greenTeamPoints.ToString();
                break;
            case Boid.TypeOfBoid.Blue:
                blueTeamPoints++;
                s_BlueTeamPointsText.text = blueTeamPoints.ToString();
                break;
            default:
                Debug.LogWarning("No existe el equipo " + team.ToString());
                break;
        }
        if(checkForWinner) {
            CheckIfOneTeamWinned();
        }
    }

    private static void CheckIfOneTeamWinned() {
        bool greenWinned = false;
        bool blueWinned = false;
        if(greenTeamPoints >= s_pointsToWin) {
            greenWinned = true;
        } else if(blueTeamPoints >= s_pointsToWin) {
            blueWinned = true;
        }
        if(greenWinned && !blueWinned) {
            s_WinnerText.gameObject.SetActive(true);
            s_WinnerText.text += GREEN_TEAM_NAME;
            checkForWinner = false;
        } else if(blueWinned && !greenWinned) {
            s_WinnerText.gameObject.SetActive(true);
            s_WinnerText.text += BLUE_TEAM_NAME;
            checkForWinner = false;
        } else if(greenWinned && blueWinned) {
            s_WinnerText.gameObject.SetActive(true);
            s_WinnerText.text = DRAW;
            checkForWinner = false;
        }
    }
}
