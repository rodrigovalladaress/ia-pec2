﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour {
    public enum TypeOfBoid {
        Green,
        Blue
    }

    [SerializeField]
    private TypeOfBoid team;

    private void AddPoint() {
        PointsManager.AddPoint(team);
    }
}